Test Acc. => 0.7768902778625488
Additional Metrics:
             precision    recall  f1-score   support

          0       0.71      0.93      0.81     47729
          1       0.46      0.11      0.17      6471
          2       0.84      0.77      0.80     77908
          3       0.81      0.88      0.84     27619
          4       0.75      0.58      0.65     27441

avg / total       0.77      0.78      0.77    187168
