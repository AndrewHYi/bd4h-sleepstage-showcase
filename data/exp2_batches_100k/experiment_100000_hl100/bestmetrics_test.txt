Test Acc. => 0.8230950236320496
Additional Metrics:
             precision    recall  f1-score   support

          0       0.89      0.87      0.88     48697
          1       0.49      0.13      0.20      6725
          2       0.84      0.86      0.85     78747
          3       0.84      0.81      0.83     24659
          4       0.70      0.81      0.75     28052

avg / total       0.82      0.82      0.82    186880
