# REDACTED: Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.
import numpy as np
import tensorflow as tf
import keras
from keras.layers import Activation, Dense
import pdb
import random
import logging
import sys
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

LAYERS_NUM = 5
SLEEP_DATA_INPUT_SIZE = 3750
FEATURE_MAP_SIZES_CONSTANT = [16, 32, 64, 128, 256]
KERNEL_SIZE_CONSTANT = 7
STRIDE_SIZE_CONSTANT = 2
FILTER_SIZES_CONSTANT = [KERNEL_SIZE_CONSTANT] * LAYERS_NUM
STRIDES_CONSTANT = [STRIDE_SIZE_CONSTANT] * LAYERS_NUM
BATCH_SIZE = 32

# REDACTED: Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.