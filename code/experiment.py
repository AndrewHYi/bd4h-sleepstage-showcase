# REDACTED: Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.

import numpy as np
import tensorflow as tf
import sklearn.metrics
import pdb
import matplotlib as plt
import random
from utils import BatchHelperTrain, BatchHelperGeneric, BatchHelperTest, EpochDoneException, FileSaver, CrossValidation
import myplot
import os
import datacleaner
import itertools

from sleepcnn import SleepCNN

np.random.seed(12345)
random.seed(12345)
tf.set_random_seed(12345)

##########################################################
############  CONFIG #####################################

SHHS_DATA_DIRS = {
    "small": "../data/shhs_small",
    "medium": "../data/shhs",
    "large": "../data/shhs_large"
}
# data_dir = SHHS_DATA_DIRS["small"]
data_dir = SHHS_DATA_DIRS["large"]
# data_dir = SHHS_DATA_DIRS["medium"]

exp_num = "00_GRADE_CHECKER_TA"



# REDACTED: Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.