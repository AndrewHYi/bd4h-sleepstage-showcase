# Project: Single-Channel EEG Sleep Stage Classification using Convolution Neural Networks

Team #51: Learning From Sleep Data
Andrew Yi (ayi36@gatech.edu)
CSE6250: Big Data Analytics in Healthcare - Spring 2019
Georgia Institute of Technology


## Prospective Employers: This is the truncated, redacted version of the project. Please contact me at andrew.yi50@gmail.com for full project report and code.

#### [Report Available Here](3_report_FINAL/report.pdf)

#### VIDEO PRESENTATION URL: https://youtu.be/ppkwOhk1FuA



##### (Note: These are private, protected files. Please see note above.)
1. PROJECT W/ Datasets (8.7 GB zipped): https://gatech.box.com/s/1v07me476sdfr2ylw4y6lcj1aau6ed3w
2. PROJECT W/O Datasets (62.5 MB zipped): https://gatech.box.com/s/o6grmllw8ngetx0hx2poxbx0u76j69d2
3. Dataset (small, edfs+xml+preproc., 3.5 GB zipped): https://gatech.box.com/s/ductyv7cmdokpzk7vq8fzd31yi4e5kyg
4. Dataset (large, preproc. only, 3.4GB zipped): https://gatech.box.com/s/w3gkf1lzts302mlf8z41byg818jw0la4

## Project Structure

<pre>
Team51_LearningFromSleepData/
├── code/
│   ├── datacleaner.py
│   ├── experiment.py
│   ├── experiment_large.py
│   ├── experiment_small.py
│   ├── myplot.py
│   ├── processdata.py
│   ├── sleepcnn.py
│   ├── utils.py
├── data/
│   ├── shhs/
│   |   ├── polysomnography/
|   │   |   ├── annotations-events-nsrr/
|   │   |   ├── edfs/
│   |   ├── preprocessed/
│   ├── shhs_large/
│   |   ├── polysomnography/
|   │   |   ├── annotations-events-nsrr/
|   │   |   ├── edfs/
│   |   ├── preprocessed/
├── custom_graphs/
</pre>


## Project Files
There are two ways to run the experiments. The first is download the FULL "Project W/ Datasets" zip - this will have the processed files used for the final project (but not the edf/annotation files) and it will also have ALL edfs/annotation/processed files used for the project draft. That is, if you download the FULL Project W/ Datasets, you can experiment with both the data processing code AND the training/testing CNN parts. However, it is several gigabytes large, so a second option is to just download the second zip, "Project W/O Datasets", and just download the datasets you want to use to run the experiments; one or both of "Dataset (small, no preprocessed)" or "Dataset (large, only preprocessed)".

******************************************************
### TLDR: For the "quickest" option to test the project:
******************************************************

1. Download "Project W/O Datasets" (this was also submitted in Canvas for your convenience).
2. Download "Dataset (small, no preprocessed)" => Unzip and place this in the CORRECT directory, as listed above in "Project Structure" (i.e. `code/shhs`).
3. From the `code/` directory (NOTE: Do *Not* run from the root directory), run `python experiment_small.py`. This will do inline preprocessing of the *small* dataset, then run train/test.
4. After the experiment is finished, you can also test the plotting code: again, from the code/ directory, run `python myplot.py`. Graphs are saved in code/custom_graphs.
5. [OPTIONAL] If you want to run `python experiment_large.py`, you will need to first download "Dataset (large, only preprocessed)", and place it in the correct directory (see "Project Structure" above). Alternatively, just download "PROJECT W/ Datasets".
6. Note: Results will be saved under `data/exp00_GRADE_CHECKER_TA/` (and printed via STDOUT)



### Explanation of files/directories: 

1. `/code` directory contains all code for dataprocessing, training, testing, batching, and creating the CNN model,
2. `/data` directory contains all experiment related output, as well as the raw sleep data edf and annotation xml files. Model checkpoints for tensorflow are also saved in this directory. Additionally, training/validation/test accuracies and results are saved in scoped folders.
3. `/custom_graphs` directory contains all graphs used for the report. Running the graphing code will override files in the directory, so please note that!.
4. `Team51_LearningFromSleepData.pdf` report pdf file,
5. `Team51_LearningFromSleepData.ppt` powerpoint file with audio.



## Requirements and Setup
All experiments were run using python 3.6.4, along the following core libraries: numpy, matplotlib, tensorflow, keras, sklearn and pyedflib (for reading edf files).


## Experiment Instructions (MUST run from /code directory, and *NOT* the root directory)
Run `python experiment_small.py`, as explained above to start/test quickly. Optionally, if you have the *FULL* datasets downloaded, you can try running either `python experiment.py` (this runs ALL hyperparameter tuning experiments mentioned in the report, and takes several hours to run) or `pytrhon experiment_large.py`. Results for experiment_small.py and experiment_large.py wil be in saved under `data/exp00_GRADE_CHECKER_TA/` (and printed via STDOUT).

Plotting code depends on having run the experiments already! 
